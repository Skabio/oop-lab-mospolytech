﻿class Notification : IComparable<Notification>
{
    public string Content { get; set; }
    public DateTime Timestamp { get; set; }

    public Notification(string content, DateTime timestamp)
    {
        Content = content;
        Timestamp = timestamp;
    }

    public int CompareTo(Notification other)
    {
        return Timestamp.CompareTo(other.Timestamp);
    }
}

