﻿class SMSNotification : Notification
{
    public string PhoneNumber { get; set; }

    public SMSNotification(string content, DateTime timestamp, string phone)
        : base(content, timestamp)
    {
        PhoneNumber = phone;
    }
}
