﻿class EmailNotification : Notification
{
    public string EmailAddress { get; set; }

    public EmailNotification(string content, DateTime timestamp, string email)
        : base(content, timestamp)
    {
        EmailAddress = email;
    }
}
