﻿using System.Text;

class NotificationContainer<T> where T : Notification, IComparable<T>
{
    private List<T> notifications = new List<T>();
    private readonly string _Cont;

    public void Add(T notification)
    {
        notifications.Add(notification);
    }

    public T Get(int index)
    {
        if (index >= 0 && index < notifications.Count)
        {
            return notifications[index];
        }
        return null;
    }

    public bool Contains(T notification)
    {
        return notifications.Contains(notification);
    }

    public void Sort()
    {
        notifications.Sort();
    }

    public void DisplayNotifications()
    {
        foreach (var notification in notifications)
        {
            Console.WriteLine($"{notification.Timestamp}: {notification.Content}");
        }
    }

    public void SaveToTextFile(string filePath)
    {
        using (StreamWriter writer = new StreamWriter(filePath, true))
        {
            foreach (var notification in notifications)
            {
                if (notification is EmailNotification)
                {
                    var emailNotification = notification as EmailNotification;
                    writer.WriteLine($"{emailNotification.Timestamp}: Email - {emailNotification.Content} to {emailNotification.EmailAddress}");
                }
                else if (notification is SMSNotification)
                {
                    var smsNotification = notification as SMSNotification;
                    writer.WriteLine($"{smsNotification.Timestamp}: SMS - {smsNotification.Content} to {smsNotification.PhoneNumber}");
                }
            }
        }
    }
    public string GetPrintable()
    {
        StringBuilder notif = new StringBuilder();
        notif.AppendLine($"Список всех сообщений {_Cont}");
        foreach (var notification in notifications)
        {
            notif.AppendLine(notification.ToString());
        }
        return notif.ToString();
    }

    public void AddNewNotification(T notification)
    {
        Add(notification);
    }

    public void RemoveNotification(string content)
    {
        notifications.RemoveAll(n => n.Content == content);
    }
    public T FindNotificationByContent(string content)
    {
        return notifications.Find(n => n.Content == content);
    }
}
