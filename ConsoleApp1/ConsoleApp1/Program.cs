﻿
class Program
{
    public static void Main()
    {
        var emailContainer = new NotificationContainer<EmailNotification>();
        var smsContainer = new NotificationContainer<SMSNotification>();



        while (true)
        {
            Console.WriteLine("Сделайте запрос:");
            Console.WriteLine("Вывести список всех сообщений - 1");
            Console.WriteLine("Найти по имени сообщения - 2");
            Console.WriteLine("Добавить сообщение - 3");
            Console.WriteLine("Удалить сообщение - 4");
            Console.WriteLine("Вывести список добавляемых сообщений - 5");
            Console.WriteLine("Выти из приложения -  6");
            int number = Convert.ToInt32(Console.ReadLine());

            switch (number)
            {
                case 1:
                    string filePath = "notifications.txt";
                    if (File.Exists(filePath))
                    {
                        using (StreamReader reader = new StreamReader(filePath))
                        {
                            string line;
                            while ((line = reader.ReadLine()) != null)
                            {
                                Console.WriteLine(line);
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("Файл 'notifications.txt' не существует.");
                    }
                    break;
                case 2:
                    Console.WriteLine("Введите название сообщения для поиска:");
                    string notifFound = Console.ReadLine();
                    Console.WriteLine("Искать enail 1");
                    Console.WriteLine("Искать SMS 2");
                    int localFound = Convert.ToInt32(Console.ReadLine());

                    if (localFound == 1)
                    {
                        var emailNotification = emailContainer.FindNotificationByContent(notifFound);
                        if (emailNotification != null)
                        {
                            Console.WriteLine($"Найдено email уведомление: {emailNotification.Content}");
                        }
                        else
                        {
                            Console.WriteLine("Email уведомление не найдено.");
                        }
                    }
                    else if (localFound == 2)
                    {
                        var smsNotification = smsContainer.FindNotificationByContent(notifFound);
                        if (smsNotification != null)
                        {
                            Console.WriteLine($"Найдено SMS уведомление: {smsNotification.Content}");
                        }
                        else
                        {
                            Console.WriteLine("SMS уведомление не найдено.");
                        }
                    }
                    break;
                case 3:
                    Console.WriteLine("Введите тип уведомления: email или sms");
                    string type = Console.ReadLine();
                    Console.WriteLine("Введите содержание уведомления:");
                    string content = Console.ReadLine();
                    Console.WriteLine("Введите день создания уведомления:");
                    int day = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Введите месяц создания уведомления:");
                    int month = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Введите год создания уведомления:");
                    int year = Convert.ToInt32(Console.ReadLine());
                    DateTime timestamp = new DateTime(year, month, day);

                    if (type == "email")
                    {
                        Console.WriteLine("Введите адрес электронной почты:");
                        string email = Console.ReadLine();
                        var newEmail = new EmailNotification(content, timestamp, email);
                        emailContainer.AddNewNotification(newEmail);
                    }
                    else if (type == "sms")
                    {
                        Console.WriteLine("Введите номер телефона:");
                        string phone = Console.ReadLine();
                        var newSMS = new SMSNotification(content, timestamp, phone);
                        smsContainer.AddNewNotification(newSMS);
                    }
                    else
                    {
                        Console.WriteLine("Неверный тип уведомления.");
                    }
                    break;
                case 4:
                    Console.WriteLine("Введите тип уведомления: email или sms");
                    string typenot = Console.ReadLine();
                    Console.WriteLine("Введите содержание");
                    string contentRem = Console.ReadLine();
                    if (typenot == "email")
                    {

                        emailContainer.RemoveNotification(contentRem);
                        Console.WriteLine("Сообщение удалено");
                    }
                    else if (typenot == "sms")
                    {
                        smsContainer.RemoveNotification(contentRem);
                        Console.WriteLine("Сообщение удалено");
                    }
                    else
                    {
                        Console.WriteLine("Неверный тип уведомления.");
                        break;
                    }
                    break;
                case 5:
                    Console.WriteLine("Введите тип уведомления: email или sms");
                    string Redtype = Console.ReadLine();
                    string save;

                    if (Redtype == "email")
                    {
                        Console.WriteLine("Добавляемые сообщения email:");
                        emailContainer.DisplayNotifications();
                        Console.WriteLine("Сохраненить сообщения?(Да или Нет)");
                        save = Console.ReadLine();
                        if (save == "Да")
                        {
                            emailContainer.SaveToTextFile("notifications.txt");
                            Console.WriteLine("Сообщения добавлены");
                        }
                        else
                            break;
                    }
                    else if (Redtype == "sms")
                    {
                        Console.WriteLine("Updated SMS Notifications:");
                        smsContainer.DisplayNotifications();
                        Console.WriteLine("Сохранение сообщения?(Да или Нет)");
                        save = Console.ReadLine();
                        if (save == "Да")
                        {
                            smsContainer.SaveToTextFile("notifications.txt");
                            Console.WriteLine("Сообщения добавлены");
                        }
                        else
                            break;
                    }
                    break;
                case 6:
                    Console.WriteLine("Работа окончена");
                    return;

            }
        }
    }
}